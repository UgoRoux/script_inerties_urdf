import math



#program BASED on https://en.wikipedia.org/wiki/List_of_moments_of_inertia
# By Yougo

class InertialCalculator(object):

    def __init__(self):
        print("InertialCaluclator Initialised...")

    def start_ask_loop(self):

        selection = "START"

        while selection != "Q":
            print("#############################")
            print("Select Geometry to Calculate:")
            print("[1]Box width(w)*depth(d)*height(h)")
            print("[2]Sphere radius(r)")
            print("[3]Cylinder radius(r)*height(h)")
            print("[4]END program")
            selection = int(input(">>"))
            self.select_action(selection)

        print("InertialCaluclator Quit...Thank you")

    def select_action(self, selection):
        print(selection)
        if selection == 1:
            mass = float(input("mass>>"))
            width = float(input("width>>"))
            depth = float(input("depth>>"))
            height = float(input("height>>"))
            self.calculate_box_inertia(m=mass, w=width, d=depth, h=height)
        elif selection == 2:
            mass = float(input("mass>>"))
            radius = float(input("radius>>"))
            self.calculate_sphere_inertia(m=mass, r=radius)
        elif selection == 3:
            mass = float(input("mass>>"))
            radius = float(input("radius>>"))
            height = float(input("height>>"))
            self.calculate_cylinder_inertia(m=mass, r=radius, h=height)
        elif selection == 4:
            print("Selected Quit")
        else:
            print("Usage: Select one of the give options")


    def calculate_box_inertia(self, m, w, d, h):
        Iw = (m/12.0)*(pow(d,2)+pow(h,2))
        Id = (m / 12.0) * (pow(w, 2) + pow(h, 2))
        Ih = (m / 12.0) * (pow(w, 2) + pow(d, 2))
        print("==========================")
        print("Result formated as URDF :")
        self.print_inertias(Iw,0.0,0.0,Id,0.0,Ih,m)



    def calculate_sphere_inertia(self, m, r):
        I = (2*m*pow(r,2))/5.0
        print("==========================")
        print("Result formated as URDF :")
        self.print_inertias(I,0.0,0.0,I,0.0,I,m)

    def calculate_cylinder_inertia(self, m, r, h):
        Ix = (m/12.0)*(3*pow(r,2)+pow(h,2))
        Iy = Ix
        Iz = (m*pow(r,2))/2.0
        print("==========================")
        print("Result formated as URDF :")
        self.print_inertias(Ix,0.0,0.0,Iy,0.0,Iz,m)

    def print_inertias(slef,xx,xy,xz,yy,yz,zz,mass):
                
            
            
        print("<inertial>")
        print("\t<origin rpy=\"0 0 0\" xyz=\"0 0 0\" />")
        print("\t<mass value=\"{:.9f}\" />".format(mass))
        print("\t<inertia ixx=\"{:.9f}\" ixy=\"{:.9f}\" ixz=\"{:.9f}\" iyy=\"{:.9f}\" iyz=\"{:.9f}\" izz=\"{:.9f}\"/>".format(xx,xy,xz,yy,yz,zz))
        print("</inertial>")


if __name__ == "__main__":
    inertial_object = InertialCalculator()
    inertial_object.start_ask_loop()